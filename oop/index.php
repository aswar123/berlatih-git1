<?php
require_once('Ape.php');
require_once('Frog.php');
require_once('animal.php');
$sheep = new Animal('shaun');
echo "Name : ". $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold blooded : " . $sheep->cold_blooded . "<br> <br>";

$kodok = new Frog("buduk");
echo "Name : ". $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold blooded : " . $kodok->cold_blooded . "<br>";
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();// "Auooo"

?>