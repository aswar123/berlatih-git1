@extends('layout.master')
@section('title')
    Halaman Detail Users
@endsection
 @push('scripts')
    <script src="{{'/Admin/plugins/datatables/jquery.dataTables.js'}}"></script>
<script src="{{'/Admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js'}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
    @endpush
    @section('content')
    <h4>Nama :  {{$cast->nama}}</h4>
    <h4>Umur :  {{$cast->umur}}</h4>
    <p>Bio : {{$cast->bio}}</p>
    @endsection