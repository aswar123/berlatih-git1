@extends('layout.master')
@section('title')
    Halaman Data Table
@endsection
 @push('scripts')
    <script src="{{'/Admin/plugins/datatables/jquery.dataTables.js'}}"></script>
<script src="{{'/Admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js'}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
    @endpush
    @section('content')
    <div>
    <h2>Tambah Data User</h2>
        <form action="/cast" method="post">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Bio</label>
                <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
    @endsection