@extends('layout.master')
@section('title')
    Halaman Edit Users
@endsection
 @push('scripts')
    <script src="{{'/Admin/plugins/datatables/jquery.dataTables.js'}}"></script>
<script src="{{'/Admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js'}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
    @endpush
    @section('content')
   <div>
        <h2>Edit Users {{$cast->nama}} Id {{$cast->id}}</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             <div class="form-group">
                <label for="umur">bio</label>
                <input type="text" class="form-control" name="umur"  value="{{$cast->umur}}"  id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             
            <div class="form-group">
                <label for="bio">bio</label>
                <textarea type="text" class="form-control" name="bio" value="{{$cast->bio}}"  id="bio" cols="30" rows="10" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                @error('')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
    @endsection