<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register() {
        return view('users.register');
    }

    public function homeUsers(Request $request) {
        $name = $request['name'];
        return view('users.homeUsers', compact('name'));
    }
}
