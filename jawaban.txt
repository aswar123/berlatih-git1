
soal 1
CREATE DATABASE myshop;

soal 2
CREATE TABLE users (id INT AUTO_INCREMET,
name VARCHAR(255),
email VARCHAR(255),
password VARCHAR(255),
PRIMARY KEY(id));

CREATE TABLE catagories (id INT AUTO_INCREMENT,
name VARCHAR(255),
PRIMARY KEY(id));

CREATE TABLE items (id INT AUTO_INCREMENT,
name VARCHAR(255),
description VARCHAR(255),
price INT,
stock INT,
catagory_id INT,
PRIMARY KEY(id),
FOREIGN KEY(catagory_id) REFERENCES categories(id));


soal 3
INSERT INTO USERS (name, email, password) VALUES('John Doe', 'john@doe.com', 'john123');
INSERT INTO USERS (name, email, password) VALUES('Jane Doe', 'jane@doe.com', 'jenita123');


INSERT INTO catagories (name) VALUES ('gadget');
INSERT INTO catagories (name) VALUES ('cloth');
INSERT INTO catagories (name) VALUES ('men');
INSERT INTO catagories (name) VALUES ('women');
INSERT INTO catagories (name) VALUES ('branded');

INSERT INTO items (name, description, price, stock, category_id) VALUES ('Sumsang b50', 'hape keren dari merek sumsang',
4000000, 100, 1); 
INSERT INTO items (name, description, price, stock, category_id) VALUES ('Uniklooh', 'baju keren dari brand ternama',
500000, 50, 2);
INSERT INTO items (name, description, price, stock, category_id) VALUES ('IMHO Watch', 'jam tangan anak yang jujur banget',
2000000, 10, 1); 

soal 4
a.
SELECT id, name, email FROM users;
b.
SELECT * FROM items WHERE price > 1000000;
c. 
SEELCT items.name, items.description, items.price, items.stock, items.category_id, catagories.kategori FROM 
items LEFT JOIN catagories ON items.category_id = catagories.id;



soal 5
UPDATE items SET price = 2500000 WHERE name = 'Sumsang b50';